package com.interview.city;

import android.support.annotation.NonNull;

//Implemented Comparable to allow Sorting
public class City implements Comparable<City> {

    public final String name;
    public final String countryName;

    public City(final String name, final String countryName) {
        this.name = name;
        this.countryName = countryName;
    }

    @Override
    public int compareTo(@NonNull City city) {
        // Added x10 to country name to add more weight in sorting
        return (countryName.compareTo(city.countryName)*10) + name.compareTo(city.name);
    }
}
