package com.interview.city;

import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;



//Asynctask to not block UI, since GetCitiesResponse has a delay thread
//Pass back to UI thread in post Execute
public  class CityLoader extends AsyncTask<Void, Void, List<City>>{

    private CityLoaded cityLoaded;
    private CityClient cityClient;

    public interface CityLoaded {
        void onCityLoaded(List<City> cities);
    }


    public CityLoader(CityLoaded cityLoaded, CityClient cityClient){
        this.cityLoaded = cityLoaded;
        this.cityClient = cityClient;
    }

    @Override
    protected List<City> doInBackground(Void... voids) {
        try {
            Log.d("tag", "loading started");
            return cityClient.getCities().cities;
        } catch (IOException e) {
            ArrayList<City> dummy = new ArrayList<>();
            return dummy;
        }
    }

    @Override
    protected void onPostExecute(List<City> cities) {
        Log.d("tag", "loading DONE");

        if(!isCancelled()){ // Check if another loading request is made
            cityLoaded.onCityLoaded(cities);
        }
    }


}
