package com.interview.city;

import java.io.IOException;


public interface CityClient {

    GetCitiesResponse getCities() throws IOException;
}
