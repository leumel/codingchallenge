package com.interview.city;

import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class CityClientImpl implements CityClient {

    private static final Random RANDOM = new Random();

    private static final Map<String, List<String>> COUNTRIES_WITH_CITIES = new HashMap<>();
    static {
        COUNTRIES_WITH_CITIES.put("Australia", Arrays.asList("Canberra", "Melbourne", "Perth", "Sydney"));
        COUNTRIES_WITH_CITIES.put("Canada", Arrays.asList("Toronto", "Vancouver", "Montreal", "Calgary"));
        COUNTRIES_WITH_CITIES.put("Ireland", Arrays.asList("Dublin", "Galway", "Kilkenny"));
        COUNTRIES_WITH_CITIES.put("Mexico", Arrays.asList("Guadalajara", "Mexico City", "Monterrey"));
        COUNTRIES_WITH_CITIES.put("Philippines", Arrays.asList("Manila", "Cebu City"));
        COUNTRIES_WITH_CITIES.put("United Kingdom", Arrays.asList("London", "Conwy", "Brighton", "Bath", "York", "Edinburgh"));
        COUNTRIES_WITH_CITIES.put("United States", Arrays.asList("San Francisco", "New York", "Boston", "Los Angeles"));
        COUNTRIES_WITH_CITIES.put("Uruguay", Arrays.asList("Montevideo", "Punta del Este", "Colonia del Sacramento"));
    }

    @Override
    public GetCitiesResponse getCities() throws IOException {
        randomDelay();
        final int cityCount = RANDOM.nextInt(30);
        final List<City> cities = new ArrayList<>(cityCount);
        for (int i = 0; i < cityCount; ++i) {
            final int randomCountryIndex = RANDOM.nextInt(COUNTRIES_WITH_CITIES.size());
            final Map.Entry<String, List<String>> countryWithCities =
                    (Map.Entry<String, List<String>>) COUNTRIES_WITH_CITIES.entrySet().toArray()[randomCountryIndex];
            final int randomCityIndex = RANDOM.nextInt(countryWithCities.getValue().size());
            final String randomCityName = countryWithCities.getValue().get(randomCityIndex);
            cities.add(new City(
                    randomCityName,
                    countryWithCities.getKey()));
        }
        return new GetCitiesResponse(cities);
    }


    private static void randomDelay() {
        try {
            Thread.sleep(RANDOM.nextInt(5000));
        } catch (InterruptedException e) {
            Log.e(CityClientImpl.class.getSimpleName(), "Interrupted");
            Thread.currentThread().interrupt();
        }
    }
}
