package com.interview.city;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public final class MainActivity extends AppCompatActivity implements CityLoader.CityLoaded, SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.rvCityList) RecyclerView rvCityList;
    @BindView(R.id.swipeLayout) SwipeRefreshLayout swipeRefreshLayout;

    private CityClient cityClient;
    private CityLoader cityLoader;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cityClient = new CityClientImpl();
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        swipeRefreshLayout.setOnRefreshListener(this);
        loadCities();
    }

    public void loadCities(){
        //run in background thread to not make UI unresponsive
        cityLoader = new CityLoader(this, cityClient);
        cityLoader.execute();
    }

    @Override
    public void onCityLoaded(List<City> cities) {
        Collections.sort(cities);
        LinkedHashMap<String, City> cityMap = new LinkedHashMap<>();

        /**
         * City Loader has a Tendency to return back duplicate items.
         * Fixed by adding an Overloaded Constructor on adapter
         * To not allow duplicates pass the Hashmap instead**/
        for(City city : cities){
            cityMap.put(city.name, city);
        }

        if (cities.size()!=0){
            rvCityList.setVisibility(View.VISIBLE);
            rvCityList.setLayoutManager(new LinearLayoutManager(this));
            /** to allow duplicates pass the List instead **/
            // rvCityList.setAdapter(new CityRecyclerViewAdapter(cities));
            rvCityList.setAdapter(new CityRecyclerViewAdapter(cityMap));
        }else {
            rvCityList.setVisibility(View.GONE);
        }

        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onRefresh() {
        cityLoader.cancel(true);
        loadCities();
    }


    /***HANDLER IMPLEMENTATION**/
    //via Handler and Thread
    // Sufficient but very dirty implementatiom

    /* private void loadCities() {
        //Run in background thread to not block UI thread
        new Handler().post(new Runnable() {
            @Override
            public void run() {
               new Thread(){
                   @Override
                   public void run() {
                       try {
                          onCityLoaded(cityClient.getCities().cities);
                       } catch (IOException e) {
                           e.printStackTrace();
                       }
                   }
               }.start();
            }
        });
    } */
}
