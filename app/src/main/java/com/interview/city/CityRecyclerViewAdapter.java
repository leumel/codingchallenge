package com.interview.city;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CityRecyclerViewAdapter extends RecyclerView.Adapter<CityRecyclerViewAdapter.CityViewHolder> {

    private List<City> cities;
    private HashMap<String, City> cityMap;
    private List<String> keys;

    CityRecyclerViewAdapter (@NonNull HashMap<String, City> cityMap){
        this.cityMap = cityMap;
        this.keys = new ArrayList<>(cityMap.keySet());
    }

    CityRecyclerViewAdapter(@NonNull  List<City> cities){
        this.cities = cities;
    }

    @Override
    public CityRecyclerViewAdapter.CityViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CityViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_city_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CityRecyclerViewAdapter.CityViewHolder holder, int position) {
        City city = null;
        City prevCity = null;

        if (cities!=null){
            city = cities.get(position);
            if (position!=0){
                prevCity = cities.get(position-1);
            }
        }else {
            city = cityMap.get(keys.get(position));
            if (position!=0){
                prevCity = cityMap.get(keys.get(position - 1));
            }
        }

        if (position == 0 || !city.countryName.equals(prevCity.countryName)){
            holder.tvCountryName.setVisibility(View.VISIBLE);
        }else{
            //Code to hide country name if view recycled is with header
            holder.tvCountryName.setVisibility(View.GONE);
        }

        holder.tvCountryName.setText(city.countryName);
        holder.tvCityName.setText(city.name);
    }

    @Override
    public int getItemCount() {
        return cities!=null ? cities.size() : cityMap.keySet().size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    class CityViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.tvCityName) TextView tvCityName;
        @BindView(R.id.tvCountryName) TextView tvCountryName;

        public CityViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
